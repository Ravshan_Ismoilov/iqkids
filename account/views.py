from .forms import LoginForm, UserRegistrationForm, UserEditForm, ProfileEditForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from .models import Profile


@login_required
def dashboard(request):
	section = "dashboard"

	return render(request, 'account/dashboard.html', locals())

@login_required
def edit(request):

    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user, data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.profile,
                                       data=request.POST,
                                       files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Sizning ma\'lumotlaringiz muvaffaqiyatli yangilandi')
        else:
            messages.error(request, 'Ma\'lumotlaringizni yangilashda xatolik sodir bo\'ldi')
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
    return render(request,'account/edit.html', locals())


def register(request):

    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)

        if user_form.is_valid():
            # Создаем нового пользователя, но пока не сохраняем в базу данных.
            new_user = user_form.save(commit=False)
            # Задаем пользователю зашифрованный пароль.
            new_user.set_password(user_form.cleaned_data['password'])
            # Сохраняем пользователя в базе данных.
            new_user.save()
            # Создание профиля пользователя.
            Profile.objects.create(user=new_user)
            return render(request,
                          'account/register_done.html',
                          {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request,'account/register.html', locals())


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(request,
                username=cd['username'],
                password=cd['password'])
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('mainapp:user_index')
            else:
                return HttpResponse('Sizning profilingiz bloklangan')
        else:
            return HttpResponse('Login yoki parolda xatolik mavjud. Iltimos qayta urunib ko\'ring')
    else:
        form = LoginForm()
        
    return render(request, 'account/login.html', {'form': form})