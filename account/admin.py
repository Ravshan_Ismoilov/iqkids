from django.utils.html import format_html
from django.contrib import admin
from .models import Profile

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'user', 'date_of_birth', 'your_photo']
    readonly_fields = 'your_photo',
    def your_photo(self, obj):
        return format_html('<img src="{}" width=150  />'.format(obj.photo.url))