from django.urls import path
from . import views

app_name = 'mainapp'

urlpatterns = [
    path('', views.mainapp, name='mainapp'),
    path('kurs/', views.user_index, name='user_index'),
]

