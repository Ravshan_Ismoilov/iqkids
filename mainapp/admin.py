from django.utils.html import format_html
from django.contrib import admin
from .models import Kurs, Test

@admin.register(Kurs)
class KursAdmin(admin.ModelAdmin):
    list_display = ['name', 'klass', 'kurs_image', 'status']
    search_fields = ('name', 'klass')
    prepopulated_fields = {'slug': ('name',)}
    # prepopulated_fields = {'slug': ('title',)}
    readonly_fields = 'kurs_image',

    def kurs_image(self, obj):
        return format_html('<img src="{}" width=150  />'.format(obj.image.url))

@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    list_display = ['title', 'category', 'status', 'kurs_photo']
    readonly_fields = 'kurs_photo',
    def kurs_photo(self, obj):
        return format_html('<img src="{}" width=150  />'.format(obj.photo.url))