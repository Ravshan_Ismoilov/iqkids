from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Kurs(models.Model):
    STATUS_CHOICES = (
        ('active', 'Active'),
        ('inactive', 'Inactive'),
    )

    name = models.CharField(max_length = 255)
    slug = models.SlugField(max_length=250)
    klass = models.CharField(max_length = 255)
    body = models.TextField()
    image = models.ImageField(upload_to='kurs/%Y/%m/%d/', blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='inactive')

    def __str__(self):
        return 'Kurs nomi: {}'.format(self.name)

class Test(models.Model):
    STATUS_CHOICES = (
        ('active', 'Active'),
        ('inactive', 'Inactive'),
    )
    DOSTUP = (
        ('open', 'Open'),
        ('close', 'Close'),
        ('decided', 'Decided'),
    )
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, unique_for_date='publish')
    photo = models.ImageField(upload_to='Test/%Y/%m/%d/', blank=True)
    category = models.ForeignKey(Kurs, on_delete=models.CASCADE)
    role = models.CharField(max_length=10, choices=DOSTUP, default='close')
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='inactive')

    def __str__(self):
        return self.title