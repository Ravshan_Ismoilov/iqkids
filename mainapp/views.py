from django.shortcuts import render
from django.contrib.auth.decorators import login_required



# Create your views here.

def mainapp(request):
    return render(request, 'index/index.html', {})


@login_required
def user_index(request):
    return render(request, 'index/user_index.html', {})